package com.yann.android;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;


public class MainActivity extends AppCompatActivity
    {
    private TextView errorConnectTextView;
    private EditText usernameEditText;
    private EditText passwordEditText;

    private Button connectBtn;

    private String username;
    private String password;
    private DataBaseManager dataBaseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        errorConnectTextView = findViewById(R.id.errorConnect);
        usernameEditText = findViewById(R.id.usernameEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        connectBtn = findViewById(R.id.connectButton);
        dataBaseManager = new DataBaseManager(getApplicationContext());

        connectBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                username = usernameEditText.getText().toString();
                password = passwordEditText.getText().toString();

                connectUser();
                }

            });
        }

        public void onApiResponse(JSONObject reponse) {
            Boolean success = null;
            String error = "";
            System.out.println("1");
            try {
                success = reponse.getBoolean("success");

                if(success == true) {
                    Intent interfaceActivity = new Intent(getApplicationContext(), QuizActivity.class);
                    interfaceActivity.putExtra("username", username);
                    startActivity(interfaceActivity);
                    finish();
                } else {
                    error = reponse.getString("error");
                    errorConnectTextView.setVisibility(View.VISIBLE);
                    errorConnectTextView.setTextColor(Color.rgb(200,0,0));
                    errorConnectTextView.setText(error);
                }
            } catch (JSONException e){
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
            }
        }

        public void connectUser(){
            String url = "http://10.0.2.2/projetandroid/connexion.php";

            Map<String, String> params = new HashMap<>();
            params.put("username", username);
            params.put("password", password);

            JSONObject parameters = new JSONObject(params);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    onApiResponse(response);
                    Toast.makeText(getApplicationContext(), "CONNEXION REUSSIE", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            });

            dataBaseManager.queue.add(jsonObjectRequest);

        }
    }