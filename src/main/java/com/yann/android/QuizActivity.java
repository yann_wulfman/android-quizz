package com.yann.android;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView NumQuestion;
    private TextView Intitule;
    private Button Prop1, Prop2, Prop3, Prop4;
    private Button Valider;

    private int NbrQuestion = QuestionReponse.question.length;
    private int Score = 0;
    private int QuestionCourante = 0;
    private String ReponseSelectionner = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        NumQuestion = findViewById(R.id.NumQuestion);
        Intitule = findViewById(R.id.Intitule);
        Prop1 = findViewById(R.id.Prop1);
        Prop2 = findViewById(R.id.Prop2);
        Prop3 = findViewById(R.id.Prop3);
        Prop4 = findViewById(R.id.Prop4);
        Valider = findViewById(R.id.Valider);

        Prop1.setOnClickListener(this);
        Prop2.setOnClickListener(this);
        Prop3.setOnClickListener(this);
        Prop4.setOnClickListener(this);
        Valider.setOnClickListener(this);

        NumQuestion.setText("Question n°1"+ QuestionCourante);

        loadNewQuestion();

    }

    @Override
    public void onClick(View view) {

        Prop1.setBackgroundColor(Color.GRAY);
        Prop2.setBackgroundColor(Color.GRAY);
        Prop3.setBackgroundColor(Color.GRAY);
        Prop4.setBackgroundColor(Color.GRAY);

        Button clickedButton = (Button) view;
        if(clickedButton.getId() == R.id.Valider){
            if(ReponseSelectionner.equals(QuestionReponse.reponse[QuestionCourante])){
                Score++;
            }
            QuestionCourante ++;
            loadNewQuestion();

        } else {
            ReponseSelectionner = clickedButton.getText().toString();
            clickedButton.setBackgroundColor(Color.rgb(127,0,255));
        }
    }

    public void loadNewQuestion(){

        if(QuestionCourante == NbrQuestion){
            finishQuiz();
            return;
        }

        Intitule.setText(QuestionReponse.question[QuestionCourante]);
        Prop1.setText(QuestionReponse.propostion[QuestionCourante][0]);
        Prop2.setText(QuestionReponse.propostion[QuestionCourante][1]);
        Prop3.setText(QuestionReponse.propostion[QuestionCourante][2]);
        Prop4.setText(QuestionReponse.propostion[QuestionCourante][3]);
    }

    public void finishQuiz() {

        new AlertDialog.Builder(this)
                .setTitle("Résultat")
                .setMessage("Votre score est de : " + Score + "/" + NbrQuestion)
                .setPositiveButton("Recommencer", (dialogInterface, i) -> restartQuiz())
                .setCancelable(false)
                .show();
    }

    public void restartQuiz(){

        Score = 0;
        QuestionCourante = 0;
        loadNewQuestion();
    }


}